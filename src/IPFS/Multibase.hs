{-# LANGUAGE OverloadedStrings #-} 

module IPFS.Multibase where

import qualified Data.Text as T
import qualified Data.ByteString.Char8 as BS

import qualified Data.ByteString.Base16      as Base16
import qualified Data.ByteString.Base32      as Base32
import qualified Data.ByteString.Base64      as Base64

-- TODO: maybe use base58string-style implementation. It is closer to Golang
-- version and more easily re-used.

data Multibase = Multibase Encoding BS.ByteString

data Encoding
  = Identity
  | Base1
  | Base2
  | Base8
  | Base10
  | Base16
  | Base16Upper
  | Base32
  | Base32Upper
  | Base32Pad
  | Base32PadUpper
  | Base32Hex
  | Base32HexUpper
  | Base32HexPad
  | Base32HexPadUpper
  | Base58Flickr
  | Base58BTC
  | Base64
  | Base64URL
  | Base64Pad
  | Base64URLPad
  deriving (Eq)

makeEncoder :: Encoding -> (BS.ByteString -> BS.ByteString) -> BS.ByteString -> BS.ByteString
makeEncoder encoding fn bs = encodingToBS encoding `BS.append` fn bs

-- TODO: add remaining encodings
encode :: Multibase -> Either String BS.ByteString
encode (Multibase Identity  bs) = Right $ makeEncoder Identity  id               bs
encode (Multibase Base16    bs) = Right $ makeEncoder Base16    Base16.encode    bs
encode (Multibase Base32    bs) = Right $ makeEncoder Base32    Base32.encode    bs
encode (Multibase Base32Hex bs) = Right $ makeEncoder Base32Hex Base32.encodeHex bs
encode (Multibase Base64    bs) = Right $ makeEncoder Base64    Base64.encode    bs
encode (Multibase Base64URL bs) = Right $ makeEncoder Base64URL Base64.encodeURL bs
encode (Multibase _         _ ) = Left "Codec not implemented"

makeDecoder :: Encoding -> (BS.ByteString -> Either String BS.ByteString) -> BS.ByteString -> Either String Multibase
makeDecoder enc fn bs = Multibase enc <$> fn bs

decode :: BS.ByteString -> Either String Multibase
decode bs =
  case encoding of
    Nothing                -> Left "Invalid encoding"
    Just Identity          -> makeDecoder Identity Right str
    Just Base1             -> Left "Encoding not implemented"
    Just Base2             -> Left "Encoding not implemented" 
    Just Base8             -> Left "Encoding not implemented"
    Just Base10            -> Left "Encoding not implemented"
    Just Base16            -> makeDecoder Base16 Base16.decode str
    Just Base16Upper       -> Left "Encoding not implemented"
    Just Base32            -> makeDecoder Base32 Base32.decode str
    Just Base32Upper       -> Left "Encoding not implemented"
    Just Base32Pad         -> Left "Encoding not implemented"
    Just Base32PadUpper    -> Left "Encoding not implemented"
    Just Base32Hex         -> makeDecoder Base32 Base32.decodeHex str
    Just Base32HexUpper    -> Left "Encoding not implemented"
    Just Base32HexPad      -> Left "Encoding not implemented"
    Just Base32HexPadUpper -> Left "Encoding not implemented"
    Just Base58Flickr      -> Left "Encoding not implemented"
    Just Base58BTC         -> Left "Encoding not implemented"
    Just Base64            -> makeDecoder Base64    Base64.decode    str
    Just Base64URL         -> makeDecoder Base64URL Base64.decodeURL str
    Just Base64Pad         -> Left "Encoding not implemented"
    Just Base64URLPad      -> Left "Encoding not implemented"
  where
    (enc, str) = BS.splitAt 1 bs
    encoding = bsToEncoding enc


encodingToBS :: Encoding -> BS.ByteString
encodingToBS Identity          = BS.singleton '\0'
encodingToBS Base1             = BS.singleton '1'
encodingToBS Base2             = BS.singleton '0'
encodingToBS Base8             = BS.singleton '7'
encodingToBS Base10            = BS.singleton '9'
encodingToBS Base16            = BS.singleton 'f'
encodingToBS Base16Upper       = BS.singleton 'F'
encodingToBS Base32            = BS.singleton 'b'
encodingToBS Base32Upper       = BS.singleton 'B'
encodingToBS Base32Pad         = BS.singleton 'c'
encodingToBS Base32PadUpper    = BS.singleton 'C'
encodingToBS Base32Hex         = BS.singleton 'v'
encodingToBS Base32HexUpper    = BS.singleton 'V'
encodingToBS Base32HexPad      = BS.singleton 't'
encodingToBS Base32HexPadUpper = BS.singleton 'T'
encodingToBS Base58Flickr      = BS.singleton 'Z'
encodingToBS Base58BTC         = BS.singleton 'z'
encodingToBS Base64            = BS.singleton 'm'
encodingToBS Base64URL         = BS.singleton 'u'
encodingToBS Base64Pad         = BS.singleton 'M'
encodingToBS Base64URLPad      = BS.singleton 'U'

bsToEncoding :: BS.ByteString -> Maybe Encoding
bsToEncoding "\0" = Just Identity
bsToEncoding "1"  = Just Base1
bsToEncoding "0"  = Just Base2
bsToEncoding "7"  = Just Base8
bsToEncoding "9"  = Just Base10
bsToEncoding "f"  = Just Base16
bsToEncoding "F"  = Just Base16Upper
bsToEncoding "b"  = Just Base32
bsToEncoding "B"  = Just Base32Upper
bsToEncoding "c"  = Just Base32Pad
bsToEncoding "C"  = Just Base32PadUpper
bsToEncoding "v"  = Just Base32Hex
bsToEncoding "V"  = Just Base32HexUpper
bsToEncoding "t"  = Just Base32HexPad
bsToEncoding "T"  = Just Base32HexPadUpper
bsToEncoding "Z"  = Just Base58Flickr
bsToEncoding "z"  = Just Base58BTC
bsToEncoding "m"  = Just Base64
bsToEncoding "u"  = Just Base64URL
bsToEncoding "M"  = Just Base64Pad
bsToEncoding "U"  = Just Base64URLPad
bsToEncoding _    = Nothing
