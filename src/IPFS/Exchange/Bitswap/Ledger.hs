{-# LANGUAGE DuplicateRecordFields #-}

module IPFS.Exchange.Bitswap.Ledger
  ( Ledger(..)
  , Receipt(..)

  , new
  , ratioValue
  , sentBytes
  , receivedBytes
  , wants
  , cancelWant
  , wantListContains
  ) where

import qualified IPFS.Peer as Peer
import qualified IPFS.Exchange.Bitswap.WantList as WantList
import qualified IPFS.CID as CID

import Data.Word
import qualified Data.Time.Clock as Time
import qualified Data.Time.Calendar as Time
import qualified Data.Map as Map
import qualified Data.ByteString.Char8 as BS
import qualified Data.Text as T

zeroTime :: Time.UTCTime
zeroTime =
  Time.UTCTime (Time.ModifiedJulianDay 0) 0

new :: Peer.ID -> Ledger
new p = Ledger
  { partner       = p
  , accounting    = Ratio 0 0
  , lastExchange  = zeroTime
  , exchangeCount = 0
  , wantList      = WantList.new
  , sentToPeer    = Map.empty
  , ref           = 0
  }

data Ratio = Ratio
  { sent :: Word64
  , recv :: Word64
  }

data Ledger = Ledger
  { partner       :: Peer.ID
  , accounting    :: Ratio
  , lastExchange  :: Time.UTCTime
  , exchangeCount :: Word64
  , wantList      :: WantList.WantList
  , sentToPeer    :: Map.Map BS.ByteString Time.UTCTime
  , ref           :: Int
  }

data Receipt = Receipt
  { peer      :: T.Text
  , value     :: Float
  , sent      :: Word64
  , recv      :: Word64
  , exchanged :: Word64
  }

ratioValue :: Ratio -> Float
ratioValue (Ratio sent recv) = fromIntegral sent / fromIntegral (1 + recv)

addSent :: Word64 -> Ratio -> Ratio
addSent n (Ratio sent recv) = Ratio (sent + n) recv

addRecv :: Word64 -> Ratio -> Ratio
addRecv n (Ratio sent recv) = Ratio sent (recv + n)

updateAccounting :: (Ratio -> Ratio) -> Ledger -> IO Ledger
updateAccounting update ledger = do
  now <- Time.getCurrentTime
  return $ ledger 
    { exchangeCount = exchangeCount ledger + 1
    , lastExchange = now
    , accounting = update (accounting ledger)
    }

sentBytes :: Word64 -> Ledger -> IO Ledger
sentBytes n = updateAccounting (addSent n)

receivedBytes :: Word64 -> Ledger -> IO Ledger
receivedBytes n = updateAccounting (addRecv n)

updateWantList :: (WantList.WantList -> WantList.WantList) -> Ledger -> Ledger
updateWantList update ledger = ledger
  { wantList = update (wantList ledger)
  }

wants :: CID.CID -> Int -> Ledger -> Ledger
wants key priority = updateWantList (WantList.add key priority)

cancelWant :: CID.CID -> Ledger -> Ledger
cancelWant key = updateWantList (WantList.remove key)

wantListContains :: CID.CID -> Ledger -> Maybe WantList.Entry
wantListContains key ledger = WantList.contains key (wantList ledger)
