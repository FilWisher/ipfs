module IPFS.Exchange.Bitswap.WantList
  ( WantList(..)
  , Entry(..)

  , new
  , len
  , add
  , addEntry
  , remove
  , contains
  , entries
  , sortedEntries
  ) where

import qualified IPFS.CID as CID
import qualified Data.Set as Set
import qualified Data.Map.Strict as Map
import qualified Data.ByteString.Char8 as BS

import Data.Word
import Data.List (sort)

-- TODO: implement threadsafe versions using STM

type WantList = Map.Map BS.ByteString Entry

data Entry = Entry
  { cid :: CID.CID
  , priority :: Int
  , sessions :: Set.Set Word64
  }
  deriving (Eq, Ord)

new :: WantList
new = Map.empty

add :: CID.CID -> Int -> WantList -> WantList
add key priority =
  Map.insert (CID.keystring key) (Entry key priority Set.empty)

addEntry :: Entry -> WantList -> WantList
addEntry entry =
  Map.insert (CID.keystring $ cid entry) entry

remove :: CID.CID -> WantList -> WantList
remove key = Map.delete (CID.keystring key)

len :: WantList -> Int
len = Map.size

contains :: CID.CID -> WantList -> Maybe Entry
contains key = Map.lookup (CID.keystring key) 

entries :: WantList -> [Entry]
entries = Map.elems

sortedEntries :: WantList -> [Entry]
sortedEntries = sort . entries
