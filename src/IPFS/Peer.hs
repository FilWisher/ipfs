module IPFS.Peer 
  ( ID(..)
  ) where

import qualified Data.ByteString.Char8 as BS

newtype ID = ID { unID :: BS.ByteString }
