{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module IPFS.CID 
  ( CID(..)

  , keystring
  ) where

import qualified Data.ByteString.Char8 as B8
import qualified Data.ByteString as ByteString (pack)
import qualified IPFS.Multihash as Multihash
import qualified IPFS.Multibase as Multibase

import Data.Word
import qualified Data.Text as T

import qualified Data.Bytes.Serial as Bytes
import qualified Data.Bytes.VarInt as Bytes
import qualified Data.Bytes.Put    as Bytes
import qualified Data.Bytes.Get    as Bytes

import Data.Aeson (FromJSON(..), ToJSON(..), (.=), (.:), object, withObject)

-- following: https://github.com/ipfs/go-cid/blob/master/cid.go
--
-- TODO: everything from `func UnmarshallJSON (:380 onwards)

data Version
  = V0
  | V1
  deriving (Eq, Ord)

data Codec
  = Raw
  | DagProtobuf
  | DagCBOR
  | GitRaw
  | EthBlock
  | EthBlockList
  | EthTxTrie
  | EthTx
  | EthTxReceiptTrie
  | EthTxReceipt
  | EthStateTrie
  | EthAccountSnapshot
  | EthStorageTrie
  | BitcoinBlock
  | BitcoinTx
  | ZcashBlock
  | ZcashTx

instance Eq Codec where
  c1 == c2 = codecToWord c1 == codecToWord c2

instance Ord Codec where
  c1 `compare` c2 = codecToWord c1 `compare` codecToWord c2

codecToWord :: Codec -> Word64
codecToWord Raw                = 0x55
codecToWord DagProtobuf        = 0x70
codecToWord DagCBOR            = 0x71
codecToWord GitRaw             = 0x78
codecToWord EthBlock           = 0x90
codecToWord EthBlockList       = 0x91
codecToWord EthTxTrie          = 0x92
codecToWord EthTx              = 0x93
codecToWord EthTxReceiptTrie   = 0x94
codecToWord EthTxReceipt       = 0x95
codecToWord EthStateTrie       = 0x96
codecToWord EthAccountSnapshot = 0x97
codecToWord EthStorageTrie     = 0x98
codecToWord BitcoinBlock       = 0xb0
codecToWord BitcoinTx          = 0xb1
codecToWord ZcashBlock         = 0xc0
codecToWord ZcashTx            = 0xc1


codecToText :: Codec -> T.Text
codecToText Raw                = "raw"
codecToText DagProtobuf        = "protobuf"
codecToText DagCBOR            = "cbor"
codecToText GitRaw             = "git-raw"
codecToText EthBlock           = "eth-block"
codecToText EthBlockList       = "eth-block-list"
codecToText EthTxTrie          = "eth-tx-trie"
codecToText EthTx              = "eth-tx"
codecToText EthTxReceiptTrie   = "eth-tx-receipt-trie"
codecToText EthTxReceipt       = "eth-tx-receipt"
codecToText EthStateTrie       = "eth-state-trie"
codecToText EthAccountSnapshot = "eth-account-snapshot"
codecToText EthStorageTrie     = "eth-storage-trie"
codecToText BitcoinBlock       = "bitcoin-block"
codecToText BitcoinTx          = "bitcoin-tx"
codecToText ZcashBlock         = "zcash-block"
codecToText ZcashTx            = "zcash-tx"

newCIDV0 :: Multihash.Multihash -> CID
newCIDV0 mhash = CID
  { version = V0
  , codec   = DagProtobuf
  , hash    = mhash
  }

newCIDV1 :: Codec -> Multihash.Multihash -> CID
newCIDV1 ctype mhash = CID
  { version = V1
  , codec   = ctype
  , hash    = mhash
  }

newPrefixV0 :: Multihash.Type -> Prefix
newPrefixV0 mhType = Prefix
  { version  = V0
  , mhType   = mhType
  , mhLength = Multihash.defaultLengths mhType
  , codec    = DagProtobuf
  }

newPrefixV1 :: Codec -> Multihash.Type -> Prefix
newPrefixV1 ctype mhType = Prefix
  { version  = V1
  , mhType   = mhType
  , mhLength = Multihash.defaultLengths mhType
  , codec    = ctype
  }

data Prefix = Prefix
  { version  :: Version
  , codec    :: Codec
  , mhType   :: Multihash.Type
  , mhLength :: Int
  }

-- TODO: implement me
data CID = CID
  { version :: Version
  , codec   :: Codec
  , hash    :: Multihash.Multihash
  }
  deriving (Ord)

-- TODO: add instances for ByteString and Text
class Parseable a b where
  parse :: a -> Either String b

instance Parseable Multihash.Multihash CID where
  parse mh = Right (newCIDV0 mh)

instance Parseable T.Text CID where
  parse = decode

instance Parseable Multibase.Multibase CID where
  parse = cast

decode :: T.Text -> Either String CID
decode str
  | T.length str < 2 = Left "CID string too short"
  | T.length str == 46 && T.take 2 str == "Qm"
      = newCIDV0 <$> Multihash.fromB58String str
  | otherwise = Multibase.decode (B8.pack . T.unpack $ str) >>= cast

-- TODO: not detecting version correctly
cast :: Multibase.Multibase -> Either String CID
cast (Multibase.Multibase _ str)
  | B8.length str == 34
  , B8.take 2 str == ByteString.pack [18, 32] =
      newCIDV0 <$> Multihash.cast str
  | otherwise = Bytes.runGetS Bytes.deserialize str

parseCIDV1 :: B8.ByteString -> Either String CID
parseCIDV1 str = undefined

-- TODO: not sure if this is accurate, need to compare Haskell VarInt
-- serialization is compatible with golang VarInt serialization
instance Bytes.Serial Codec where
  serialize   = Bytes.serialize . Bytes.VarInt . codecToWord
  deserialize = do
    b <- (Bytes.unVarInt :: Bytes.VarInt Word64 -> Word64) <$> Bytes.deserialize
    case b of
      0x55 -> return Raw
      0x70 -> return DagProtobuf
      0x71 -> return DagCBOR
      0x78 -> return GitRaw
      0x90 -> return EthBlock
      0x91 -> return EthBlockList
      0x92 -> return EthTxTrie
      0x93 -> return EthTx
      0x94 -> return EthTxReceiptTrie
      0x95 -> return EthTxReceipt
      0x96 -> return EthStateTrie
      0x97 -> return EthAccountSnapshot
      0x98 -> return EthStorageTrie
      0xb0 -> return BitcoinBlock
      0xb1 -> return BitcoinTx
      0xc0 -> return ZcashBlock
      0xc1 -> return ZcashTx
      _    -> fail "Unrecognized Codec"

instance Bytes.Serial Version where
  serialize V0 = Bytes.serialize (Bytes.VarInt (0 :: Word64))
  deserialize = do
    v <- (Bytes.unVarInt :: Bytes.VarInt Word64 -> Word64) <$> Bytes.deserialize
    case v of
      0 -> return V0
      1 -> return V1
      _ -> fail "Unrecognized version"


-- TODO: this isn't accurate, v1 serialization is different to v2
instance Bytes.Serial CID where
  serialize (CID version codec mhash) = do
    Bytes.serialize version
    Bytes.serialize codec
    Bytes.serialize mhash
  deserialize = do
    v <- Bytes.deserialize
    case v of
      V0 -> newCIDV0 <$> Bytes.deserialize
      V1 -> newCIDV1 <$> Bytes.deserialize <*> Bytes.deserialize

instance Eq CID where
  CID v1 c1 h1 == CID v2 c2 h2 = v1 == v2 && c1 == c2 && h1 == h2

toString :: CID -> Either String T.Text
toString cid = 
  (T.pack . B8.unpack) <$> case cid of
    (CID V0 _ hash)     -> Right $ Multihash.b58String hash
    cid@(CID V1 _ hash) -> Multibase.encode (Multibase.Multibase Multibase.Base58BTC $ bytes cid)

toStringOfBase :: CID -> Multibase.Encoding -> Either String B8.ByteString
toStringOfBase (CID V0 _ mhash) base
  | base == Multibase.Base58BTC = Right $ Multihash.b58String mhash
  | otherwise = Left "Invalid encoding"
toStringOfBase cid@(CID V1 _ mhash) base = Multibase.encode (Multibase.Multibase base $ bytes cid)

bytes :: CID -> B8.ByteString
bytes = Bytes.runPutS . Bytes.serialize

keystring :: CID -> B8.ByteString
keystring key = ""


instance ToJSON CID where
  toJSON cid = case toString cid of
    Left err -> error err
    Right bs -> object [ "/" .= bs ]

instance FromJSON CID where
  parseJSON = withObject "CID" $ \v ->
    parse =<< v .: "/"
    where
      parse bs = case decode bs of
        Left err -> fail err
        Right b -> return b
