{-# LANGUAGE OverloadedStrings #-}

module IPFS.Multihash
  ( Multihash(..)
  , Type(..)

  , defaultLengths
  , fromB58String
  , b58String
  , cast
  , toBytes
  ) where

-- https://github.com/multiformats/go-multihash/blob/master/multihash.go

import qualified Data.ByteString.Char8 as B8
import qualified Data.Text as T

import qualified Data.Bytes.Serial as Bytes

-- TODO: implement me
newtype Multihash = Multihash { bytes :: B8.ByteString }
  deriving (Eq, Ord)

data Type
  = Type

instance Bytes.Serial Multihash where
  serialize = undefined
  deserialize = undefined

defaultLengths :: Type -> Int
defaultLengths Type = 0

fromB58String :: T.Text -> Either String Multihash
fromB58String str = Left "TODO"

b58String :: Multihash -> B8.ByteString
b58String str = "TODO"

cast :: B8.ByteString -> Either String Multihash
cast str = Left "TODO"

toBytes :: Multihash -> B8.ByteString
toBytes = undefined
